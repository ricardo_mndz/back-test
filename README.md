# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* JavaScript knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations

- JSON-P
>Es una técnica de JavaScript para solicitar datos cargando una etiqueta <script>.  
- CDI
> define el mecanismo para resolver dependencias entre servicios dentro del estándar JEE
- JPA
> Es un framework del lenguaje de programación Java que maneja datos relacionales en aplicaciones usando la Plataforma Java en sus ediciones Standard (Java SE) y Enterprise (Java EE).
- JAX-RS
> Es una API del lenguaje de programación Java que proporciona soporte en la creación de servicios web de acuerdo con el estilo arquitectónico Representational State Transfer (REST)

3. Which of the following is/are not an application server?

* Helidon 
* Quarkus
* WebSphere
* Tomcat
* KumuluzEE
> Helidon No, Quarkus No, Tomcat No, KumuluzEE No.

4. In your opinion what's the main benefit of moving from Java 8 to Java 11
> The main benefit is the memory usage (heap) that java 11 does.

5. In your opinion what's the main benefit of using TypeScript over JavaScript
> the main benefit of typescript is that we can use classes and thus have a greater abstraction,
> that is, orient it to objects more easily

7. What's the main difference between OpenJDK and HotSpot
> The difference is the business features offered by hotspot

8. If no database is configured? Will you be able to run this project? Why?
> Yes, the project could be executed. However, we will have exceptions with our APIs for not having a configured DB

## Development tasks

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11 and  NodeJS 16
   (![](task's/task0.png))
1. (easy) Build this project on a regular CLI over OpenJDK 11
   ![](task's/task1.png)
2. (easy) Run this project using an IDE/Editor of your choice
   ![](task's/task2.png)
3. (medium) This project has been created using Java EE APIs, please identify at least four APIs, bump it to Java EE 8 and Java 11, later run it over regular Payara Application Server 
    ![](task's/task3.png)
4. (medium) Execute the movie endpoint operations using a client -e.g. Postman-, if needed please also check/fix the code to be REST compliant
    ![](task's/task4.png)
5. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer
   > https://bitbucket.org/ricardo_mndz/fronttest/src/master/

6. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods
    ![](task's/task6.png)

7. (hard) Based on `MovieRepository` Integration Test, please create an integration test using [RestAssured](https://rest-assured.io/) and `MovieController`

8. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test
    ![](task's/task8.png)
9. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Oracle Helidon](https://helidon.io/). Do it and don't port Integration Tests 

