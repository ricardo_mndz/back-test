package com.nabenik.controller;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.repository.ActorRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/actor")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActorController {

    @Inject
    ActorRepository actorRepository;

    @GET
    public List<Actor> listAll(){
        return actorRepository.listAll();
    }

    @GET
    @Path("/{id}")
    public Actor findById(@PathParam("id") Long id){
        return actorRepository.findById(id);
    }

    @POST
    public Response create(Actor actor){
        actorRepository.create(actor);
        return Response.created(URI.create("/LolActor")).build();
    }

    @PUT
    @Path("/{id}")
    public Response update(@PathParam("id") Long id, Actor actor){
        actorRepository.update(actor);
        return Response.created(URI.create("/LolActor")).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id){
        Actor movie = actorRepository.findById(id);
        actorRepository.delete(movie);
        return Response.ok().build();
    }
}
