package com.nabenik.repository;


import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static WebArchive createDeployment(){
        WebArchive war = ShrinkWrap.create(WebArchive.class)
                .addClass(Movie.class)
                .addClass(Actor.class)
                .addClass(EntityManagerProducer.class)
                .addClass(ActorRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");

        System.out.println(war.toString(true));

        return war;
    }

    @Test
    public void create() {
        Actor actor = new Actor("Ricardo", "Quetzaltenango", "28-06" );
        actorRepository.create(actor);
        System.out.println("Actor's name " + actor.getName());

        assertNotNull(actor.getName());
    }

    @Test
    public void update() {
        Actor actor = new Actor("Ricardo", "Quetzaltenango", "28-06" );
        actorRepository.create(actor);
        actor.setName("Augusto");
        actorRepository.update(actor);
        System.out.println("Actor's name " + actor.getName());

        assertNotNull(actor.getName());
    }

    @Test
    public void delete() {
        Actor actor = new Actor("Ricardo", "Quetzaltenango", "28-06" );
        actorRepository.delete(actor);
        assertNull(actor.getActorId());
    }

    @Test
    public void findById() {
        Actor actor = new Actor("Ricardo", "Quetzaltenango", "28-06" );
        actorRepository.create(actor);

        Actor myActor = actorRepository.findById(actor.getActorId());

        assertNotNull(myActor.getActorId());
    }

    @Test
    public void listAll() {
        List<Actor> actorList = actorRepository.listAll();

        System.out.println("My actor list has: " + actorList.size() + "actors");

        assertEquals(0,actorList.size());
    }
}
